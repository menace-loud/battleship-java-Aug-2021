[![Build status](https://psdstewards.visualstudio.com/PSD/_apis/build/status/proscrumdev.battleship-java-CI)](https://psdstewards.visualstudio.com/PSD/_build/latest?definitionId=15)

# Battleship Java

A simple game of Battleship, written in Java.

# Getting started

This project requires a Java JDK 8 or higher. To prepare to work with it, pick one of these
options:

## Run locally

Run battleship with Gradle

```bash
./gradlew run
```

Execute tests with Gradle

```bash
./gradlew test
```

## Docker

If you don't want to install anything Java-related on your system, you can
run the game inside Docker instead.

### Run a Docker Container from the Image

```bash
docker run -it -v ${PWD}:/battleship -w /battleship openjdk:15 bash
```

This will run a Docker container with your battleship case study mounted into it. The container will run in interactive mode and you can execute Gradle commands from the shell (see examples below).

# Launching the game

```bash
./gradlew run
```

# Running the Tests

```
./gradlew test
```

# Новая функциональность
В каждой игре корабли компьютера размещаются на поле случайно в соответствии с правилами:
- Корабли не могут пересекаться
- Корабли не могут быть за пределами поля
- Все корабли правильного размера
- Корабли сплошные по вертикали или горизонтали, пробелы недопустимы
Игрок не видит позиции кораблей компьютера
