package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.Mockito;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringWriter;

import static org.mockito.Mockito.verify;

@Execution(ExecutionMode.CONCURRENT)
public class MainTest {

    @Test
    public void testParsePosition() {
        Position actual = Main.parsePosition("A1");
        Position expected = new Position(Letter.A, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testParsePosition2() {
        //given
        Position expected = new Position(Letter.B, 1);
        //when
        Position actual = Main.parsePosition("B1");
        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testHitColor() {
        //given
        ColoredPrinter console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

        ColoredPrinter spyConsole = Mockito.spy(console);

        Main.printHitResult(spyConsole, true);

        verify(spyConsole).setForegroundColor(Ansi.FColor.RED);
        verify(spyConsole).setForegroundColor(Ansi.FColor.WHITE);
    }

    @Test
    public void testMissColor() {
        //given
        ColoredPrinter console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

        ColoredPrinter spyConsole = Mockito.spy(console);

        Main.printHitResult(spyConsole, false);

        verify(spyConsole).setForegroundColor(Ansi.FColor.BLUE);
        verify(spyConsole).setForegroundColor(Ansi.FColor.WHITE);
    }
}
