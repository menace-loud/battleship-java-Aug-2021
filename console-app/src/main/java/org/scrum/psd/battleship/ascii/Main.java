package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {
    public static final Ansi.FColor DEFAULT_FOREGROUND_COLOR = Ansi.FColor.WHITE;
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console;

    public static void main(String[] args) {
        main0(new Randomizer());
    }

    public static void main0(Randomizer randomizer) {
        PrintHeader();
        InitializeGame(randomizer);
        StartGame();
    }

    private static void PrintHeader() {
        console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(DEFAULT_FOREGROUND_COLOR).build();

        console.setForegroundColor(Ansi.FColor.MAGENTA);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.setForegroundColor(Ansi.FColor.WHITE);
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");

        do {
            console.println("");
            console.println("Player, it's your turn");
            console.println("Enter coordinates for your shot :");
            Position position = parsePosition(scanner.next());
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                beep();

                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
            }

            printHitResult(console, isHit);

            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);
            console.println("");
            console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss"));
            if (isHit) {
                beep();

                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");

            }
        } while (true);
    }

    static void printHitResult(ColoredPrinter console, boolean isHit) {
        if (isHit) {
            console.setForegroundColor(Ansi.FColor.RED);
            console.println("Yeah ! Nice hit !");
            console.setForegroundColor(Ansi.FColor.WHITE);
        } else {
            console.setForegroundColor(Ansi.FColor.BLUE);
            console.println("Miss");
            console.setForegroundColor(Ansi.FColor.WHITE);
        }
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        return new Position(letter, number);
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }

    private static void InitializeGame(Randomizer randomizer) {
        InitializeMyFleet();
        InitializeEnemyFleet(randomizer);
    }


    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");


        if (Boolean.parseBoolean(System.getProperty("debug"))) {
            myFleet.get(0).getPositions().add(new Position(Letter.B, 4));
            myFleet.get(0).getPositions().add(new Position(Letter.B, 5));
            myFleet.get(0).getPositions().add(new Position(Letter.B, 6));
            myFleet.get(0).getPositions().add(new Position(Letter.B, 7));
            myFleet.get(0).getPositions().add(new Position(Letter.B, 8));

            myFleet.get(1).getPositions().add(new Position(Letter.E, 6));
            myFleet.get(1).getPositions().add(new Position(Letter.E, 7));
            myFleet.get(1).getPositions().add(new Position(Letter.E, 8));
            myFleet.get(1).getPositions().add(new Position(Letter.E, 9));

            myFleet.get(2).getPositions().add(new Position(Letter.A, 3));
            myFleet.get(2).getPositions().add(new Position(Letter.B, 3));
            myFleet.get(2).getPositions().add(new Position(Letter.C, 3));

            myFleet.get(3).getPositions().add(new Position(Letter.F, 8));
            myFleet.get(3).getPositions().add(new Position(Letter.G, 8));
            myFleet.get(3).getPositions().add(new Position(Letter.H, 8));

            myFleet.get(4).getPositions().add(new Position(Letter.C, 5));
            myFleet.get(4).getPositions().add(new Position(Letter.C, 6));
        } else {
            for (Ship ship : myFleet) {
                console.println("");
                console.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
                for (int i = 1; i <= ship.getSize(); i++) {
                    console.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));
                    String positionInput = scanner.next();
                    ship.addPosition(positionInput);
                }
            }
        }

    }

    public static void messageWithColor(Color color, ColoredPrinter console, String... messages) {

        console.setForegroundColor(Ansi.FColor.RED);

        Stream.of(messages).forEach(console::println);
        console.setForegroundColor(DEFAULT_FOREGROUND_COLOR);
    }

    private static void InitializeEnemyFleet(Randomizer randomizer) {
        enemyFleet = GameController.initializeShips();
        int number = randomizer.getRandomFleetNumber();
        switch(number){
            case 0: {
                enemyFleet1(enemyFleet);
                break;
            }
            case 1: {
                enemyFleet2(enemyFleet);
                break;
            }
            case 2: {
                enemyFleet3(enemyFleet);
                break;
            }
            case 3: {
                enemyFleet4(enemyFleet);
                break;
            }
            case 4: {
                enemyFleet5(enemyFleet);
                break;
            }
        }
    }

    private static void enemyFleet1(List<Ship> enemyFleet) {
        if (Boolean.parseBoolean(System.getProperty("debug"))) {
            console.println("enemy fleet 1");
        }

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7)); //UNIQUE
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 9));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static void enemyFleet2(List<Ship> enemyFleet) {
        if (Boolean.parseBoolean(System.getProperty("debug"))) {
            console.println("enemy fleet 2: Igor");
        }

        enemyFleet.get(0).getPositions().add(new Position(Letter.D, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.F, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.G, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 6)); //UNIQUE

        enemyFleet.get(1).getPositions().add(new Position(Letter.A, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.A, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.A, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.A, 8));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 1));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 2));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 3));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static void enemyFleet3(List<Ship> enemyFleet) {
        if (Boolean.parseBoolean(System.getProperty("debug"))) {
            console.println("enemy fleet 3: Alex");
        }

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.C, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.E, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.F, 4));

        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 3));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 6));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 4));
        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 5));
        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 6));

        enemyFleet.get(3).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(3).getPositions().add(new Position(Letter.C, 3));
        enemyFleet.get(3).getPositions().add(new Position(Letter.D, 3));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 8)); //UNIQUE
        enemyFleet.get(4).getPositions().add(new Position(Letter.D, 8));
    }

    private static void enemyFleet4(List<Ship> enemyFleet) {
        if (Boolean.parseBoolean(System.getProperty("debug"))) {
            console.println("enemy fleet 4: Vadim");
        }

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 2)); //UNIQUE
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));

        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.F, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 4));

        enemyFleet.get(2).getPositions().add(new Position(Letter.D, 6));
        enemyFleet.get(2).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(2).getPositions().add(new Position(Letter.F, 6));

        enemyFleet.get(3).getPositions().add(new Position(Letter.D, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 1));
        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 2));
    }

    private static void enemyFleet5(List<Ship> enemyFleet) {
        if (Boolean.parseBoolean(System.getProperty("debug"))) {
            console.println("enemy fleet 5: Nikita");
        }

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.C, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.E, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.F, 4));

        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 3));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.G, 6));

        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.D, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.A, 4));
        enemyFleet.get(3).getPositions().add(new Position(Letter.A, 5));
        enemyFleet.get(3).getPositions().add(new Position(Letter.A, 6));

        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 7)); //UNIQUE
        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 8));
    }
}
